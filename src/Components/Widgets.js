import { Layout, Row, Col, Card, Typography, Space } from "antd";
import {
  LeftOutlined,
  RightOutlined,
  BookOutlined,
  MoreOutlined,
  TagsOutlined,
} from "@ant-design/icons";

const { Sider } = Layout;
const { Text } = Typography;

export const WidgetOne = () => {
  return (
    <Card size="small" style={{ width: 300 }} className="custom_card">
      <Space size="large">
        <BookOutlined className="custom_icon_2" />
        <Layout style={{ width: "160px" }} className="white_background">
          <Text className="bold_text">General meeting</Text>
          <Text className="custom_grey_color">10 - 11 am</Text>
        </Layout>
        <MoreOutlined />
      </Space>
    </Card>
  );
};

export const WidgetTwo = () => {
  return (
    <Card size="small" style={{ width: 300 }} className="custom_card">
      <Space size="large">
        <TagsOutlined className="custom_icon_2 custom_green_color" />
        <Layout style={{ width: "160px" }} className="white_background">
          <Text className="bold_text">Unit marketing</Text>
          <Text className="custom_grey_color">12 - 1 pm</Text>
        </Layout>
        <MoreOutlined />
      </Space>
    </Card>
  );
};

const Widgets = () => {
  return (
    <Sider className="white_background" width={350}>
      <Row style={{ textAlign: "center", padding: "30px 0px 30px" }}>
        <Col className="custom_grey_color" span={7}>
          <LeftOutlined />
        </Col>
        <Col span={10} className="big_text bold_text">
          Today, 10 June
        </Col>
        <Col span={7} className="custom_grey_color">
          <RightOutlined />
        </Col>
      </Row>
      <Layout
        className="white_background aligned_center"
        style={{ justifyContent: "flex-start" }}
      >
        <WidgetOne />
        <WidgetTwo />
        <WidgetOne />
        <WidgetTwo />
        <WidgetOne />
        <WidgetTwo />
      </Layout>
    </Sider>
  );
};

export default Widgets;
