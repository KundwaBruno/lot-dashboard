import { useState } from "react";
import { Layout, Menu, Typography } from "antd";
import { Link } from "react-router-dom";
import {
  HomeFilled,
  CalendarFilled,
  UserOutlined,
  SettingOutlined,
  ProfileOutlined,
} from "@ant-design/icons";

const { Sider } = Layout;
const { Text } = Typography;

const SideBar = () => {
  const [collapsed, setCollapsed] = useState();

  return (
    <Sider
      width={350}
      className="white_background"
      style={{
        borderRight: "1px solid rgb(225, 235, 255)",
        height: "auto",
      }}
      collapsible
      collapsed={collapsed}
      onCollapse={setCollapsed}
    >
      <div className="profile_div">
        <img
          className="profile_image"
          src="https://images.pexels.com/photos/573238/pexels-photo-573238.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
          alt="Profile"
        />
        <div style={{ opacity: collapsed ? 0 : 1 }}>
          <Text className="big_text bold_text ">Kundwa Bruno</Text>
          <Text className="custom_grey_color" style={{ display: "block" }}>
            Admin
          </Text>
        </div>
      </div>
      <Menu theme="light" defaultSelectedKeys={["1"]} mode="inline">
        <Menu.Item key="1" icon={<HomeFilled className="custom_icon_2" />}>
          <Link to="/">Home</Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<CalendarFilled className="custom_icon_2" />}>
          <Link to="/appointments">Appointments</Link>
        </Menu.Item>
        <Menu.Item icon={<UserOutlined className="custom_icon_2" />}>
          <Link to="/staffs">Staffs</Link>
        </Menu.Item>
        <Menu.Item icon={<ProfileOutlined className="custom_icon_2" />}>
          <Link to="/reports">Reports</Link>
        </Menu.Item>
        <Menu.Item icon={<SettingOutlined className="custom_icon_2" />}>
          <Link to="/settings">Settings</Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
};
export default SideBar;
