import {
  LineChart,
  BarChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Bar,
} from "recharts";
const dataBar = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
  },
];

const data = [
  { name: "Lorem", uv: 0, pv: 600, amt: 2400 },
  { name: "Lorem", uv: 300, pv: 100, amt: 2400 },
  { name: "Lorem", uv: 350, pv: 350, amt: 2400 },
  { name: "Lorem", uv: 390, pv: 800, amt: 2400 },
  { name: "Lorem", uv: 500, pv: 200, amt: 2400 },
  { name: "Lorem", uv: 200, pv: 300, amt: 2400 },
  { name: "Lorem", uv: 5, pv: 400, amt: 2400 },
  { name: "Lorem", uv: 600, pv: 200, amt: 2400 },
];

const BarGraph = () => (
  <BarChart width={600} height={500} data={dataBar}>
    <CartesianGrid strokeDasharray="3 3" />
    <XAxis dataKey="name" />
    <YAxis />
    <Tooltip />
    <Legend />
    <Bar dataKey="pv" fill="#8884d8" />
    <Bar dataKey="uv" fill="#82ca9d" />
  </BarChart>
);
const LineGraph = () => (
  <LineChart
    width={1000}
    height={500}
    data={data}
    margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
  >
    <Line type="monotone" dataKey="uv" stroke="#1fc933" />
    <Line type="monotone" dataKey="pv" stroke="#1B27AF" />
    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
    <XAxis dataKey="name" />
    <YAxis />
    <Tooltip />
  </LineChart>
);
const Chart = ({ type }) => {
  return (
    <>
      {type === "line" && <LineGraph />}
      {type === "bar" && <BarGraph />}
    </>
  );
};

export default Chart;
