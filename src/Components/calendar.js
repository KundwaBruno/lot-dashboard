import React, { useState } from "react";
import Calendar from "react-calendar";
import { Layout, Row, Col, Card, Typography, Space } from "antd";
import "react-calendar/dist/Calendar.css";
import "../styles/utils.scss";
import "../styles/colors.scss";

function MyApp() {
  const [value, onChange] = useState(new Date());

  return (
    <Card className="custom_card aligned_center">
      <Calendar onChange={onChange} value={value} />
    </Card>
  );
}

export default MyApp;
