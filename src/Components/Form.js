import { useState } from "react";
import { Typography, Button, Input, Checkbox } from "antd";
import { useFormik } from "formik";
import validate from "../helpers/validations";
import { useDispatch } from "react-redux";
import { signIn } from "../Redux/Actions/auth";
import "../styles/utils.scss";

const { Text } = Typography;

const buttonStyle = {
  backgroundColor: "#1b27af",
  borderRadius: "4px",
  height: "50px",
  border: "none",
  margin: "20px 0px 20px",
};

const RenderInputs = ({ input, onChange, formik }) => {
  const values = formik.values;
  const errors = formik.errors;
  const touched = formik.touched;
  switch (input.type) {
    case "password":
      return (
        <div className="custom_input">
          <Text className="big_text custom_grey_color">{input.label}*</Text>
          <Input
            name={input.name}
            onChange={onChange}
            onBlur={formik.handleBlur}
            value={values[input.name]}
            type="password"
            size="large"
            placeholder={input.placeholder}
          />
          {touched[input.name] && errors[input.name] && (
            <div>{errors[input.name]}</div>
          )}
        </div>
      );
    case "checkbox":
      return (
        <>
          <Checkbox
            name={input.name}
            onChange={onChange}
            onBlur={formik.handleBlur}
            checked={values[input.name]}
          >
            {input.label}
          </Checkbox>
          {touched[input.name] && errors[input.name] && (
            <div className="custom_red_color">{errors[input.name]}</div>
          )}
        </>
      );
    default:
      return (
        <div className="custom_input">
          <Text className="big_text custom_grey_color">{input.label}</Text>
          <Input
            name={input.name}
            onChange={onChange}
            onBlur={formik.handleBlur}
            value={values[input.name]}
            size="large"
            placeholder={input.placeholder}
          />
          {touched[input.name] && errors[input.name] && (
            <div>{errors[input.name]}</div>
          )}
        </div>
      );
  }
};

const signInAct = async (dispatch, values, setLoading) => {
  setTimeout(async () => {
    await dispatch(signIn(values));
    setLoading(false);
  }, 1000);
};

const FormComponent = ({ inputs, isLogin }) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      names: "",
      email: "",
      password: "",
      terms: false,
    },
    validate,
    onSubmit: async (values) => {
      setLoading(true);
      await signInAct(dispatch, values, setLoading);
    },
  });
  return (
    <div>
      {inputs.map((input, index) => {
        return (
          <RenderInputs
            key={index}
            formik={formik}
            onChange={formik.handleChange}
            input={input}
          />
        );
      })}
      <Button
        onClick={formik.handleSubmit}
        style={buttonStyle}
        type="primary"
        size="large"
        block
        loading={loading}
      >
        {isLogin ? "Login" : "Sign up"}
      </Button>
    </div>
  );
};

export default FormComponent;
