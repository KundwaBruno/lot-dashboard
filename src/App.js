import React from "react";
import loginSignup from "./Pages/loginSignup";
import Overview from "./Pages/overview";
import { Layout, Typography, Row, Col, Input } from "antd";
import { LogoutOutlined, SearchOutlined, BulbFilled } from "@ant-design/icons";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SideBar from "./Components/sideBar";
import Widgets from "./Components/Widgets";
import { signOut } from "./Redux/Actions/auth";
import { useSelector, useDispatch } from "react-redux";
import Appointments from "./Pages/appointments";

const { Header, Content } = Layout;
const { Text } = Typography;

const App = () => {
  // const savedUser = localStorage.getItem("user");
  const auth = useSelector((state) => state.auth.value);
  const dispatch = useDispatch();

  return (
    <>
      <Router>
        {!auth.success && (
          <Switch>
            <Route path="/" exact component={loginSignup} />
          </Switch>
        )}
        {auth.success && (
          <Layout>
            <SideBar />
            <Layout>
              <Header className="slider_header">
                <Row style={{ width: "100%" }}>
                  <Col span={7}>
                    <Text className="header_text bold_text custom_blue_color">
                      Overview
                    </Text>
                  </Col>
                  <Col span={10}>
                    <Input
                      style={{ width: "100%" }}
                      size="medium"
                      placeholder="Search for patients"
                      className="custom_inline_input"
                      prefix={<SearchOutlined />}
                    />
                  </Col>
                  <Col span={7}>
                    <div className="custom_icon white_background align_icons">
                      <BulbFilled className="custom_icon_2 custom_black_color" />
                      <LogoutOutlined
                        onClick={() => dispatch(signOut())}
                        className="custom_icon_2 custom_black_color"
                      />
                    </div>
                  </Col>
                </Row>
              </Header>
              <Layout className="white_background">
                <Content>
                  <Switch>
                    <Route path="/" exact component={Overview} />
                    <Route
                      path="/appointments"
                      exact
                      component={Appointments}
                    />
                  </Switch>
                </Content>
                <Widgets />
              </Layout>
            </Layout>
          </Layout>
        )}
      </Router>
    </>
  );
};

export default App;
