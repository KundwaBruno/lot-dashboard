import { configureStore } from "@reduxjs/toolkit";
import authActions from "./Actions/auth";

export default configureStore({
  reducer: {
    auth: authActions,
  },
});
