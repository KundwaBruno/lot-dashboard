const onChangeHandler = (e, credentials, setCredentials) => {
  const { name } = e.target;
  const { value } = e.target;
  setCredentials({ ...credentials, [name]: value });
};

export default onChangeHandler;
