const validate = (values) => {
  const errors = {};

  if (!values.names) {
    errors.names = "Required";
  } else if (values.names.length > 15) {
    errors.names = "Name must be 15 characters or less";
  }

  if (!values.password) {
    errors.password = "Required";
  } else if (values.password.length < 8) {
    errors.password = "Must be atleast 8 characters";
  }

  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  if (!values.terms) {
    errors.terms = "Please review the terms and conditions";
  }

  return errors;
};

export default validate;
