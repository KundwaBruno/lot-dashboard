import { Layout, Menu, Space, Typography, Input, Row, Col } from "antd";
import { useState } from "react";
import Calendar from "../Components/calendar";
import { WidgetOne, WidgetTwo } from "../Components/Widgets";
import "../styles/utils.scss";
import "../styles/colors.scss";

const { Title } = Typography;

const Appointments = () => {
  return (
    <Layout
      tyle={{ width: "92%", marginTop: "26px", margin: "auto" }}
      className="white_background"
    >
      <Space
        size={20}
        direction="vertical"
        style={{ width: "80%", margin: "auto", marginTop: "10px" }}
      >
        <Title level={2} className="big_text">
          Calendar
        </Title>
        <Calendar />
        <Layout className="white_background">
          <Title level={2} className="big_text">
            June main events
          </Title>
          <Space size={20}>
            <WidgetOne />
            <WidgetTwo />
            <WidgetOne />
          </Space>
        </Layout>
      </Space>
    </Layout>
  );
};

export default Appointments;
