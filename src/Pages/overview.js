import { Layout, Card, Space, Typography, Input, Row, Col } from "antd";
import { useState } from "react";
import Chart from "../Components/chart";
import "../styles/utils.scss";
import "../styles/colors.scss";

const { Title, Text } = Typography;

const Overview = () => {
  return (
    <Layout
      className="white_background custom_overflow"
      style={{ width: "92%", marginTop: "26px", margin: "auto" }}
    >
      <Space direction="vertical" size={20} style={{ marginTop: "10px" }}>
        <Title level={2} className="big_text aligned_center">
          Welcome to Lot,
        </Title>
        <Card className="custom_card aligned_center">
          <Title level={4} style={{ fontSize: "20px", marginLeft: "60px" }}>
            Success map
          </Title>
          <Chart type="line" />
        </Card>
        <Space
          className="aligned_center"
          style={{ flexDirection: "row" }}
          direction="horizontal"
        >
          <Card className="custom_card" style={{ backgroundColor: "#1b27af" }}>
            <Title className="custom_white_color" level={2}>
              EMPLOYERS
            </Title>
            <Title className="custom_cyan_color">36</Title>
            <Text className="custom_white_color">
              Employers find their employees through us
            </Text>
          </Card>
          <Card className="custom_card" style={{ backgroundColor: "#1b27af" }}>
            <Title className="custom_white_color" level={2}>
              JOBS
            </Title>
            <Title className="custom_cyan_color">3,690</Title>
            <Text className="custom_white_color">
              Employers find their employees through us
            </Text>
          </Card>
          <Card className="custom_card" style={{ backgroundColor: "#1b27af" }}>
            <Title className="custom_white_color" level={2}>
              ONGOING PROJECTS
            </Title>
            <Title className="custom_cyan_color">36</Title>
            <Text className="custom_white_color">
              Employers find their employees through us
            </Text>
          </Card>
        </Space>
        <Card className="custom_card aligned_center">
          <Title level={4} style={{ fontSize: "20px", marginLeft: "60px" }}>
            Success map
          </Title>
          <Chart type="bar" />
        </Card>
      </Space>
    </Layout>
  );
};

export default Overview;
