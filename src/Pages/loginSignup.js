/* eslint-disable jsx-a11y/anchor-is-valid */
import { useState } from "react";
import "../styles/utils.scss";
import "../styles/colors.scss";
import FormComponent from "../Components/Form";
import { Row, Col, Typography, Divider, Space } from "antd";
import { GoogleOutlined } from "@ant-design/icons";

const { Title, Text } = Typography;

const loginInputs = [
  {
    label: "Email",
    name: "email",
    placeholder: "Enter your email address",
    type: "text",
  },
  {
    label: "Password",
    name: "password",
    placeholder: "Enter your password",
    type: "password",
  },
];

const signupInputs = [
  {
    label: "Full Name",
    name: "names",
    placeholder: "Ex: John doe",
    type: "text",
  },
  {
    label: "Email",
    name: "email",
    placeholder: "ex: johndoe@gmail.com",
    type: "text",
  },
  {
    label: "Password",
    name: "password",
    placeholder: "Enter password",
    type: "password",
  },
  {
    label: "I agree to terms and conditions",
    name: "terms",
    placeholder: "",
    type: "checkbox",
  },
];

const LoginSignup = () => {
  const [isLogin, setIslogin] = useState(true);
  return (
    <Row>
      <Col style={{ height: "100vh", position: "relative" }} span={14}>
        <img className="custom_image" src="loginBg.jpg" alt="Office" />
        <div className="custom_blue_overlay">
          <img
            className="custom_image"
            style={{ width: "30%", height: "auto", marginBottom: "40px" }}
            src="logo.svg"
            alt="LOT logo"
          />
          <Title
            style={{
              color: "white",
              fontWeight: "lighter",
            }}
            level={3}
            italic
          >
            “Let the countdown begin”
          </Title>
        </div>
      </Col>
      <Col span={10}>
        <div className="aligned_center">
          <div style={{ width: "50%" }}>
            <div className="header_text custom_blue_color">
              {isLogin ? "Log In" : "Sign up"}
            </div>
            <div className="big_text custom_grey_color">
              {isLogin &&
                `we're glad you are back! Now fill in the details and dive in.`}
              {!isLogin &&
                `We're glad you are here! Now fill in the details to get started.`}
            </div>
            <Divider />
          </div>
          <FormComponent
            inputs={isLogin ? loginInputs : signupInputs}
            isLogin={isLogin}
          />
          {!isLogin && (
            <div style={{ width: "42%", height: "auto" }}>
              <Divider className="custom_grey_color">or</Divider>
              <Space
                className="aligned_center"
                style={{
                  flexDirection: "row",
                  height: "auto",
                  margin: "0px 0px 20px",
                }}
                size="large"
              >
                <GoogleOutlined className="custom_icon" />{" "}
                <Text className="aligned_center bold_text">
                  Sign up with google
                </Text>
              </Space>
            </div>
          )}
          <div className="aligned_center" style={{ height: "auto" }}>
            <p className="custom_grey_color">
              {isLogin ? "Dont have an account?" : "Already a member ?"}
              <span
                href=""
                onClick={() => setIslogin(!isLogin)}
                className="custom_link"
              >
                {isLogin ? "Sign up" : "Login"}
              </span>
            </p>
          </div>
        </div>
      </Col>
    </Row>
  );
};

export default LoginSignup;
